$:.push('/home/vagrant/bin/gen-rb')

require 'thrift'
require 'calculator_service'

begin
  port =  ARGV[0] || 9090
  transport = Thrift::BufferedTransport.new(Thrift::Socket.new('127.0.0.1', 9090))
  protocol = Thrift::BinaryProtocol.new(transport)
  client = CalculatorService::Client.new(protocol)

  transport.open()
  puts "$ Cliente inciado"
  while true
    print "$ "
    command = gets.chomp.to_s
    if command == "exit"
      break
    end
    regex = /(...)\s([0-9])\s([0-9])/
    if regex.match command
      args = command.strip.split(" ")
      case args[0]
      when "add"
        result = client.add(Integer(args[1]), Integer(args[2]))
        puts result
      when "subtract"
        result = client.subtract(Integer(args[1]), Integer(args[2]))
        puts result
      when "multiply"
        result = client.multiply(Integer(args[1]), Integer(args[2]))
        puts result
      when "divide"
        if args[2] == "0"
          puts "Can't divide by 0"
        else
          result = client.divide(Integer(args[1]), Integer(args[2]))
          puts result
        end
      else
        puts "Invalid command"
      end
    end
  end
rescue Thrift::Exception => tx
    print 'Thrift::Exception: ', tx.message, "\n"
end

transport.close