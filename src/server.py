import glob
import sys
sys.path.append('gen-py')

from calculator_service import CalculatorService
from calculator_service.ttypes import InvalidOperation

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

class CalculatorServiceHandler:
    def __init__(self):
        self.log = {}

    def add(self, num1, num2):
        return num1 + num2

    def subtract(self, num1, num2):
        return num1 - num2

    def multiply(self, num1, num2):
        return num1 * num2

    def divide(self, num1, num2):
        return num1 / num2

handler = CalculatorServiceHandler()
procesor = CalculatorService.Processor(handler)
transport = TSocket.TServerSocket(host='127.0.0.1', port=9090)
tfactory = TTransport.TBufferedTransportFactory()
pfactory = TBinaryProtocol.TBinaryProtocolFactory()

server = TServer.TSimpleServer(procesor, transport, tfactory, pfactory)
server.serve()