exception InvalidOperation {
    1: string message
}

service CalculatorService {
    i32 add( 1: i32 num1, 2: i32 num2 ) throws (1: InvalidOperation invalidOperation),
    i32 subtract( 1: i32 num1, 2: i32 num2 ) throws (1: InvalidOperation invalidOperation),
    i32 multiply( 1: i32 num1, 2: i32 num2 ) throws (1: InvalidOperation invalidOperation),
    i32 divide( 1: i32 num1, 2: i32 num2 ) throws (1: InvalidOperation invalidOperation)
}